package threesolid;

//Workable implements IWorker so that
//Single Responsibility Principle is observed.
//Alex Martin
public class Workable extends Worker implements IWorker{
	public String work() {
	return "I'm working, So Workable!";
	}
}
