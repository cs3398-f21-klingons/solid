package threesolid;

//Robot Extends Worker to follow Open Closed Principle
//Joshua Lopez
public class Robot extends Worker implements IWorker {
		private String name = "Ben";

	  	public String getName() 
	  	{
	    	return name;
	  	}

	  	public void setName(String name) 
	  	{
	      	this.name = name;
	  	}

	  	public String work() 
	  	{  

	  		if (name == "") 
	    	{
	       		return "Please insert girder!";
	    	}
	    	else 
	    	{
	       		return name + " is working very hard!";
	    	}
		}

	      public Boolean addpositive(int a, int b)
	    {
	      if ((a+b) > 0)
	        return(true);
	      return(false);
	    }
	}
