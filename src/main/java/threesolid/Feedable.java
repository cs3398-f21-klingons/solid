

package threesolid;
//Feedable implements IWorker so that
//Single Responsibility Principle is observed.
//Alex Martin
public class Feedable extends Worker implements IWorker{

 	public String work() {
 		return "I'm eating!";
 	}
}
