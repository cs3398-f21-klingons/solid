/*
Separated IWorker into IWorker and IFeedable to
follow interface segregation principle and allow
for Robot class to implement only work() without eat()
Michael Mondragon
 */
package threesolid;

  
interface IWorker extends IFeedable, IWorkable {
}

interface IWorkable {
	public String work();
}

interface IFeedable{
	public String eat();
}