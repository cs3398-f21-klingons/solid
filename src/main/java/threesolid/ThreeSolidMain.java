//Classes were moved outside of ThreeSolidMain for SRP

package threesolid;

import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;



public class ThreeSolidMain
{   
   
   public static Manager tsManager = new Manager();

   // The entry main() method
   public static void main(String[] args) 
   {
	   Manager m = new Manager();
	   m.setWorker(new Workable());
	   m.manage();
 
      try 
      {
         System.out.format("Starting ... \n");               
      } 
      catch (Exception main_except)
      {
         main_except.printStackTrace();
      }

            try 
      {
         System.out.format("Stopping ... \n");               
      } 
      catch (Exception main_except)
      {
         main_except.printStackTrace();
      }

      System.exit(0);

   }
 }

//Adam defined Worker and SuperWorker in another class file


class Manager {
	IWorker worker;

	public void Manager() {

	}
	public void setWorker(Workable w) {
		worker=w;
	}

	public void manage() {
		worker.work();
	}
}
