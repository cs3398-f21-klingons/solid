//SuperWorker is moved to a separate file for Single Responsibility Principle
//ThreeSolidMain had a lot going on
//Adam McBay
package threesolid;
import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
import java.io.*;

import threesolid.IWorker;

//SuperWorker extends Worker in order to follow Open Closed Principle.
//Worker is open to extension by new classes, closed to modification.
//Josh Lopez

class SuperWorker extends Worker implements IWorker{
	public String work() {
		return "I'm a super worker!";
	}

	public String eat() {
		return "I'm eating a super healthy meal";
	}
}